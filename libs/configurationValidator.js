var _ = require('underscore');

var configValidator = function(){};

// Array of config values that must be present with a value for the app to run
var existsChecks = [
  "name",
  "host",
  "port"
  // add required config items here
];

configValidator.IsValid = function(){
  var isValid = true;

  _.each(existsChecks, function(key){
    if(!nconf.get(key)){
      isValid = false;
    }
  });

  return isValid;
}

configValidator.GetErrors = function(){
  var errors = [];

  _.each(existsChecks, function(key){
    if(!nconf.get(key) && !_.findWhere(errors, { "key" : key })){
      errors.push({
        "key":key,
        "errorReason": "Value does not exist in config."
      });
    }
  });

  return errors;
}

module.exports = configValidator;