var path = require("path");

/**
 * Loads configuration into nconf global obj based on a base file name
 * @param  {string} configFileBaseName
 * @return {void}
 */
var configurationLoader = function(configFileBaseName){
  if(!configFileBaseName){
    throw new Error('Parameter configFileBaseName is required to load configuration.');
  }

  /** Default to 'development' in case it is not supplied */
  nconf.defaults({ 'NODE_ENV': 'development' });

  /** Load environment config from file system */
  var envConfig = configFileBaseName + '.' + nconf.get('NODE_ENV') + '.json';
  nconf.file({
    file: envConfig,
    dir: path.join(__dirname,'..', 'config'),
    search: true
  });

  // Load default configuration from file system
  var defaultConfigPath = path.join(__dirname,'..', 'config', configFileBaseName + '.json');
  nconf.file('default', defaultConfigPath);

  // Read in any command line args for overrides
  nconf.argv();

  nconf.set('envConfig', envConfig);
  nconf.set('defaultConfig', configFileBaseName +  '.json');

  return;
};

module.exports = configurationLoader;