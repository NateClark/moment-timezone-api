var winston = require('winston');

module.exports = function(options){
  options = options || nconf.get('logger');

  //Defaults
  var defaultLevel = 'info';

  // prepare some custom log levels
  var customLevels = {
    levels: {
      debug: 0,
      info: 1,
      warn: 2,
      error: 3,
      fatal: 4
    },
    colors: {
      debug: 'cyan',
      info: 'green',
      warn: 'yellow',
      error: 'red',
      fatal: 'red'
    }
  };

  // set the coloring
  winston.addColors(customLevels.colors);

  var transports = [];

  //Load up what we have.
  if(options){
    if(options.console) {
      transports.push(getConsoleLogger(options.console));
    }
    if(options.file) {
      transports.push(getFileLogger(options.file));
    }
  }

  //Make sure we have something
  if(transports.length === 0){
    console.log("No valid logging options found. Loading default logging.");
    transports.push(getFileLogger({}));
  }

  function getConsoleLogger(options){
    return (new (winston.transports.Console)({
      level: (options.level)? options.level : defaultLevel,
      levels: customLevels.levels,
      timestamp: true,
      colorize: true
    }));
  }

  function getFileLogger(options){
    //File Defaults
    var default_file_fileName = options.fileName || 'logs/log.txt';
    var default_file_maxSize = options.maxFileSize || 2097152;
    var default_file_maxFiles = options.maxNumberFiles || 10;
    var default_file_formatAsJSON = options.formatAsJSON || false;
    var timestamp = true;
    var level = options.level || defaultLevel;

    return (new (winston.transports.File)({
      filename: default_file_fileName,
      maxsize: default_file_maxSize,
      maxfiles: default_file_maxFiles,
      json: default_file_formatAsJSON,
      timestamp: true,
      level: level,
      levels: customLevels.levels
    }));
  }

  // create the logger
  var output = new (winston.Logger)({
    levels: customLevels.levels,
    transports: transports
  });

  return output;
}