var os = require('os'),
  moment = require('moment'),
  bytes = require('bytes'),
  exec = require('child_process').exec;

/**
* Controller that defines the model to be used for the debug router
*
* @module debugController
*/

/**
* @constructor
* @alias module:debugController
*/
var debugController = function(){};

/**
 * Route controller for debug page
 * @param  {Function} req
 * @param  {Function} res
 * @param  {Function} next
 * @return {Function}
 */
debugController.debug = function(req, res, next){
  if(req.params.smile && req.params.smile == 'b10833e9-e12d-4f71-b5ef-0b647f9dca27' && (nconf.get('NODE_ENV') != 'production' || nconf.get('settings:forceDebug'))){
    var child = exec("git describe", function(error, stdout, stderr){
      options.gitDescribe = stdout && !stderr ?  stdout.replace('\n','') : 'Not available';
      options.os = os;
      options.moment = moment;
      options.bytes = bytes;
      res.render('misc/debug', options);
    });
  }
  else{
    return next();
  }
}

module.exports = debugController;