var notFoundController = function(){};

notFoundController.notFound = function(req, res, next) {
    res.status(404);

    var options = {};
    options.title = "Resource Not Found";
    options.url = req.url;

    // respond with html page
    if (req.accepts('html')) {
      res.render('misc/404', options);
      return;
    }
    else if (req.accepts('json')) {
      // respond with json
      res.send({ error: 'Resource not found' });
      return;
    }
    else{
      // default to plain-text. send()
      res.type('txt').send('Resource not found');
      return;
    }
};

module.exports = notFoundController;