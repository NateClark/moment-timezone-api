var errorController = function(){};

errorController.error = function (err, req, res, next) {
  console.error('Unhandled ' + appName + ' application exception.', err);

  res.status(500);

  var options = {};

  // respond with html page
  if (req.accepts('html')) {
    options.title = "Unexpected Error";
    options.url = req.url;

    options.errorStack = (nconf.get('settings:showErrors') ? err.stack : '');    

    return res.render('misc/500', options);
  }
  // respond with json
  else if (req.accepts('json')) {
    return res.send({ error: 'Unhandled exception. See logs for details.' });
  }
  // default to plain-text. send()
  return res.type('txt').send('Unhandled exception. See logs for details.');
};

module.exports = errorController;