// Global requires
_ = require('underscore');

// Global variables
appName = name  = "<set by configuration>";
options = {};
configName = 'moment-timezone-api';

var express = require("express"),
  debug = require('debug')('http'),
  http = require("http"),
  path = require("path"),
  notFoundController = require('./controllers/notFoundController'),
  errorController = require('./controllers/errorController'),
  configurationValidator = require('./libs/configurationValidator'),  
  app = express();

nconf = require('nconf').env([ 'NODE_ENV' ]);
require('./libs/configurationLoader')(configName);

logger = require('./libs/customLogger')();

appName = nconf.get('name');

app.configure(function() {
  app.set("name", appName);
  app.set("port", nconf.get('port') || process.env.PORT || 3000);
  app.set("host", nconf.get('hostname') || process.env.IP);
  app.set("views", __dirname + "/views");
  app.set("view engine", "jade");
  app.use(express.logger("dev"));
  if(nconf.get('compression:enabled')){
    app.use(express.compress());
  }
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.static(__dirname + '/public'), { maxAge: 86400000 });
  app.use(app.router);
  app.use(errorController.error); //catch unhandled errors, throw 500
  app.use(notFoundController.notFound); //no route mapped, throw 404
});

app.configure("development", function() {
  return app.use(express.errorHandler());
});

require("./routes/all")(app, options);
require("./routes")(app, options);

// Lastly, validate config
if(!configurationValidator.IsValid()){
  var errors = configurationValidator.GetErrors();

  // Let the user know which config files were evaluated
  logger.info('Using config files: [' + nconf.get('defaultConfig') + ', ' + nconf.get('envConfig') + ']');

  _.each(errors, function(err){
    logger.error('[' + err.key + '] ' + err.errorReason);
  });

  logger.error('Application is improperly configured. See errors above.');

  // Exit app with error because app is not configured properly
  process.exit(1);
}

http.createServer(app).listen(app.get("port"), app.get("host"), function () {
  logAppSummary();
  return;
});

function logAppSummary(){
  logger.info('Environment info:', nconf.get('NODE_ENV'));
  logger.info('Using config files: [' + nconf.get('defaultConfig') + ', ' + nconf.get('envConfig') + ']');
  if(nconf.get('compression:enabled')){
    logger.info('GZip compression is enabled.');
  }
  else{
    logger.warn('GZip compression is disabled. This should be enabled in a production environment.');
  }
  logger.info('Oven preheated. Meatloaf and tater tots set to bake.');
  logger.info('Express server listening on host and port: ' + nconf.get('host') + ':' + nconf.get('port'));
}