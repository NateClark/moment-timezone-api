var pageController = require('../controllers/pageController');

module.exports = function(app, options){
  // Useful for debug
  app.get('/', pageController.home);
};