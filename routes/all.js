module.exports = function(app, options){
  app.all('*', function(req, res, next){
    // Perform any work that needs to be done each request

    options.Globals = {};

    options.Globals.ApplicationDisplayName = nconf.get('name');

    return next();
  });

  // Useful for health monitoring
  app.all('/heartbeat', function(req, res, next){
    res.status(200);
    return res.send('OK');
  });
};