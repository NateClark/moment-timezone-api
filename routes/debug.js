var debugController = require('../controllers/debugController');

module.exports = function(app, options){
  // Useful for debug
  app.get('/debug/:smile', debugController.debug);
};